﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;


namespace VotaUAMysql
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IService1" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IService1
    {

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "BuscarFacultad/{codigo}", ResponseFormat = WebMessageFormat.Json)]
        Clases.Facultad BuscarFacultad(string codigo);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "CrearFacultad/{nombre}", ResponseFormat = WebMessageFormat.Json)]
        Boolean CrearFacultad(string nombre);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "EliminarFacultad/{codigo}", ResponseFormat = WebMessageFormat.Json)]
        Boolean EliminarFacultad(string codigo);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ModificarFacultad/{codigo}/{nombre}", ResponseFormat = WebMessageFormat.Json)]
        Boolean ModificarFacultad(string codigo, string nombre);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "CrearPrograma/{nombre}/{facultad}", ResponseFormat = WebMessageFormat.Json)]
        Boolean CrearPrograma(string nombre, string facultad);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ListarProgramas", ResponseFormat = WebMessageFormat.Json)]
        String ListarProgramas();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "BuscarPrograma/{codigo}", ResponseFormat = WebMessageFormat.Json)]
        String BuscarPrograma(string codigo);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ModificarPrograma/{codigo}/{nombre}/{facultad}", ResponseFormat = WebMessageFormat.Json)]
        Boolean ModificarPrograma(string codigo, string nombre, string facultad);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "EliminarPrograma/{codigo}", ResponseFormat = WebMessageFormat.Json)]
        Boolean EliminarPrograma(string codigo);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "CrearTipoUsuario/{nombre}/{descripcion}", ResponseFormat = WebMessageFormat.Json)]
        Boolean CrearTipoUsuario(string nombre, string descripcion);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ListarTiposUsuario", ResponseFormat = WebMessageFormat.Json)]
        String ListarTiposUsuario();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "BuscarTipoUsuario/{codigo}", ResponseFormat = WebMessageFormat.Json)]
        String BuscarTipoUsuario(string codigo);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ModificarTipoUsuario/{codigo}/{nombre}/{descripcion}", ResponseFormat = WebMessageFormat.Json)]
        Boolean ModificarTipoUsuario(string codigo, string nombre, string descripcion);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "EliminarTipoUsuario/{codigo}", ResponseFormat = WebMessageFormat.Json)]
        Boolean EliminarTipoUsuario(string codigo);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "CrearRol/{nombre}", ResponseFormat = WebMessageFormat.Json)]
        Boolean CrearRol(string nombre);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ListarRoles", ResponseFormat = WebMessageFormat.Json)]
        String ListarRoles();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "BuscarRol/{codigo}", ResponseFormat = WebMessageFormat.Json)]
        String BuscarRol(string codigo);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ModificarRol/{codigo}/{nombre}", ResponseFormat = WebMessageFormat.Json)]
        Boolean ModificarRol(string codigo, string nombre);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "EliminarRol/{codigo}", ResponseFormat = WebMessageFormat.Json)]
        Boolean EliminarRol(string codigo);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "CrearEstadoUsuario/{nombre}", ResponseFormat = WebMessageFormat.Json)]
        Boolean CrearEstadoUsuario(string nombre);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ListarEstadosUsuario", ResponseFormat = WebMessageFormat.Json)]
        String ListarEstadosUsuario();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "BuscarEstadoUsuario/{codigo}", ResponseFormat = WebMessageFormat.Json)]
        String BuscarEstadoUsuario(string codigo);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ModificarEstadoUsuario/{codigo}/{nombre}", ResponseFormat = WebMessageFormat.Json)]
        Boolean ModificarEstadoUsuario(string codigo, string nombre);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "EliminarEstadoUsuario/{codigo}", ResponseFormat = WebMessageFormat.Json)]
        Boolean EliminarEstadoUsuario(string codigo);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "CrearUsuario/{codigo}/{nombre}/{apellido1}/{apellido2}/{username}/{pass}/{programa}/{tipo_usuario}/{estado}/{rol}", ResponseFormat = WebMessageFormat.Json)]
        Boolean CrearUsuario(string codigo, string nombre, string apellido1, string apellido2, string username, string pass, string programa, string tipo_usuario, string estado, string rol);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ListarUsuarios", ResponseFormat = WebMessageFormat.Json)]
        String ListarUsuarios();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "BuscarUsuario/{codigo}", ResponseFormat = WebMessageFormat.Json)]
        String BuscarUsuario(string codigo);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ModificarUsuario/{codigo}/{nombre}/{apellido1}/{apellido2}/{pass}/{programa}/{tipousuario}/{estado}/{rol}", ResponseFormat = WebMessageFormat.Json)]
        Boolean ModificarUsuario(string codigo, string nombre, string apellido1, string apellido2, string pass, string programa, string tipousuario, string estado, string rol);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "EliminarUsuario/{codigo}", ResponseFormat = WebMessageFormat.Json)]
        Boolean EliminarUsuario(string codigo);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Login/{username}/{pass}", ResponseFormat = WebMessageFormat.Json)]
        Boolean Login(string username, string pass);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "CrearVotacion/{nombre}/{descripcion}/{fecha_inicio}/{fecha_final}/{tipo_usuario}", ResponseFormat = WebMessageFormat.Json)]
        Boolean CrearVotacion(string nombre, string descripcion, string fecha_inicio, string fecha_final, string tipo_usuario);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "EliminarVotacion/{codigo}", ResponseFormat = WebMessageFormat.Json)]
        Boolean EliminarVotacion(string codigo);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ModificarVotacion/{codigo}/{nombre}/{descripcion}/{fecha_inicio}/{fecha_final}/{tipo_usuario}", ResponseFormat = WebMessageFormat.Json)]
        Boolean ModificarVotacion(string codigo, string nombre, string descripcion, string fecha_inicio, string fecha_final, string tipo_usuario);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "AgregarCandidato/{usuario}/{votacion}", ResponseFormat = WebMessageFormat.Json)]
        Boolean AgregarCandidato(string usuario, string votacion);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "EliminarCandidato/{usuario}/{votacion}", ResponseFormat = WebMessageFormat.Json)]
        Boolean EliminarCandidato(string usuario, string votacion);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Votar/{usuario}/{votacion}", ResponseFormat = WebMessageFormat.Json)]
        Boolean Votar(string usuario, string votacion);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ProbarConexion/", ResponseFormat = WebMessageFormat.Json)]
        String ProbarConexion();


    }


    // Utilice un contrato de datos, como se ilustra en el ejemplo siguiente, para agregar tipos compuestos a las operaciones de servicio.
    
}
