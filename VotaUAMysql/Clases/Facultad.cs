﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VotaUAMysql.Clases
{
    public class Facultad
    {

        private int codigo;
        public int Codigo
        {
            get { return codigo; }
            set { codigo = value; }
        }

        private string nombre;
        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }


    }
}