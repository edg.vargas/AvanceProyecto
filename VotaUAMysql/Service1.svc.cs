﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using Newtonsoft.Json; 

namespace VotaUAMysql
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Service1" en el código, en svc y en el archivo de configuración.
    // NOTE: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Service1.svc o Service1.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class Service1 : IService1
    {

        List<MySqlParameter> lista = new List<MySqlParameter>();


        public Clases.Facultad BuscarFacultad(string codigo)
        {

            Clases.Facultad fac = new Clases.Facultad();

            DataSet Res = Conexion.Conectar.EjecutarSelectMysql("select * from facultad where cod_facultad = " + codigo + ";");
            fac.Codigo = Convert.ToInt32(Res.Tables[0].Rows[0][0].ToString());
            fac.Nombre = Res.Tables[0].Rows[0][1].ToString();
            

            return fac; 

        }


      

        public bool CrearFacultad(string nombre)
        {
            //List<MySqlParameter> lista = new List<MySqlParameter>();

            string sql = "insert into facultad (nombre_facultad) values ('" + nombre + "');";

            try
            {
                Conexion.Conectar.EjecutarOperacion(sql, lista, System.Data.CommandType.Text);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }


        public bool EliminarFacultad(string codigo)
        {
            
            string sql = "delete from facultad where cod_facultad = " + codigo + ";";

            try
            {
                Conexion.Conectar.EjecutarOperacion(sql, lista, System.Data.CommandType.Text);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }


        public bool ModificarFacultad(string codigo, string nombre)
        {
            string sql = "update facultad set nombre_facultad = '"+nombre+"' where cod_facultad = "+codigo+";";

            try
            {
                Conexion.Conectar.EjecutarOperacion(sql, lista, System.Data.CommandType.Text);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }


        public bool CrearPrograma(string nombre, string facultad)
        {
            string sql = "insert into programa (nombre_programa, FK_facultad) values ('" + nombre + "', "+facultad+");";

            try
            {
                Conexion.Conectar.EjecutarOperacion(sql, lista, System.Data.CommandType.Text);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }


        public String ListarProgramas()
        {

            DataSet Res = Conexion.Conectar.EjecutarSelectMysql("select pro.cod_programa, pro.nombre_programa, fac.cod_facultad, fac.nombre_facultad " +
                "from programa pro inner join facultad fac on pro.FK_facultad = fac.cod_facultad; "); 

            
            DataTable tab = Res.Tables[0];
            //string result = JsonConvert.SerializeObject(DatatableToDictionary(tab, "cod_programa"), Newtonsoft.Json.Formatting.Indented);
            //string result = JsonConvert.SerializeObject(DatatableToDictionary(tab, "cod_programa")); 
            return JsonConvert.SerializeObject(tab, Formatting.None); 
            
        }


        //public Dictionary<string, Dictionary<string, object>> DatatableToDictionary(DataTable dt, string id)
        //{
        //    var cols = dt.Columns.Cast<DataColumn>().Where(c => c.ColumnName != id);
        //    return dt.Rows.Cast<DataRow>()
        //             .ToDictionary(r => r[id].ToString(),
        //                           r => cols.ToDictionary(c => c.ColumnName, c => r[c.ColumnName]));
        //}



        public string BuscarPrograma(string codigo)
        {
            DataSet Res = Conexion.Conectar.EjecutarSelectMysql("select pro.cod_programa, pro.nombre_programa, fac.cod_facultad, fac.nombre_facultad " +
                "from programa pro inner join facultad fac on pro.FK_facultad = fac.cod_facultad and pro.cod_programa = " + codigo + ";"); 


            DataTable tab = Res.Tables[0];
            
            return JsonConvert.SerializeObject(tab, Formatting.None); 
        }


        public bool ModificarPrograma(string codigo, string nombre, string facultad)
        {
            string sql = "update programa set nombre_programa = '" + nombre + "', FK_facultad = "+facultad+" where cod_programa = " + codigo + ";";

            try
            {
                Conexion.Conectar.EjecutarOperacion(sql, lista, System.Data.CommandType.Text);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }


        public bool EliminarPrograma(string codigo)
        {
            string sql = "delete from programa where cod_programa = " + codigo + ";";

            try
            {
                Conexion.Conectar.EjecutarOperacion(sql, lista, System.Data.CommandType.Text);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }


        public bool CrearTipoUsuario(string nombre, string descripcion)
        {
            string sql = "insert into tipo_usuario (nombre_tipo_usuario, descripcion) values ('" + nombre + "', '" + descripcion + "');";

            try
            {
                Conexion.Conectar.EjecutarOperacion(sql, lista, System.Data.CommandType.Text);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }


        public string ListarTiposUsuario()
        {
            DataSet Res = Conexion.Conectar.EjecutarSelectMysql("select * from tipo_usuario;");
            DataTable tab = Res.Tables[0];
            return JsonConvert.SerializeObject(tab, Formatting.None); 
        }


        public string BuscarTipoUsuario(string codigo)
        {
            DataSet Res = Conexion.Conectar.EjecutarSelectMysql("select * from tipo_usuario where cod_tipo_usuario = "+codigo+";");
            DataTable tab = Res.Tables[0];
            return JsonConvert.SerializeObject(tab, Formatting.None);
        }


        public bool ModificarTipoUsuario(string codigo, string nombre, string descripcion)
        {
            string sql = "update tipo_usuario set nombre_tipo_usuario = '" + nombre + "', descripcion = '" + descripcion + "' where cod_tipo_usuario = " + codigo + ";";

            try
            {
                Conexion.Conectar.EjecutarOperacion(sql, lista, System.Data.CommandType.Text);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }


        public bool EliminarTipoUsuario(string codigo)
        {
            string sql = "delete from tipo_usuario where cod_tipo_usuario = " + codigo + ";";

            try
            {
                Conexion.Conectar.EjecutarOperacion(sql, lista, System.Data.CommandType.Text);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }


        public bool CrearRol(string nombre)
        {
            string sql = "insert into rol (nombre_rol) values ('" + nombre + "');";

            try
            {
                Conexion.Conectar.EjecutarOperacion(sql, lista, System.Data.CommandType.Text);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }


        public string ListarRoles()
        {
            DataSet Res = Conexion.Conectar.EjecutarSelectMysql("select * from rol;");
            DataTable tab = Res.Tables[0];
            return JsonConvert.SerializeObject(tab, Formatting.None);
        }


        public string BuscarRol(string codigo)
        {
            DataSet Res = Conexion.Conectar.EjecutarSelectMysql("select * from rol where cod_rol = " + codigo + ";");
            DataTable tab = Res.Tables[0];
            return JsonConvert.SerializeObject(tab, Formatting.None);
        }


        public bool ModificarRol(string codigo, string nombre)
        {
            string sql = "update rol set nombre_rol = '" + nombre + "' where cod_rol = " + codigo + ";";

            try
            {
                Conexion.Conectar.EjecutarOperacion(sql, lista, System.Data.CommandType.Text);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }


        public bool EliminarRol(string codigo)
        {
            string sql = "delete from rol where cod_rol = " + codigo + ";";

            try
            {
                Conexion.Conectar.EjecutarOperacion(sql, lista, System.Data.CommandType.Text);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }


        public bool CrearEstadoUsuario(string nombre)
        {
            string sql = "insert into estado_usuario (nombre_estado_usuario) values ('" + nombre + "');";

            try
            {
                Conexion.Conectar.EjecutarOperacion(sql, lista, System.Data.CommandType.Text);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool CrearUsuario(string codigo, string nombre, string apellido1, string apellido2, string username, string pass, string programa, string tipo_usuario, string estado, string rol)
        {
            string sql = "insert into usuario values (" + codigo + ", '"+nombre+"', '"+apellido1+"', '"+apellido2+"', '"+username+"', '"+pass+"', "+programa+","+tipo_usuario+","+estado+","+rol+");";

            try
            {
                Conexion.Conectar.EjecutarOperacion(sql, lista, System.Data.CommandType.Text);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }


        public string ListarEstadosUsuario()
        {
            DataSet Res = Conexion.Conectar.EjecutarSelectMysql("select * from estado_usuario;");
            DataTable tab = Res.Tables[0];
            return JsonConvert.SerializeObject(tab, Formatting.None);
        }


        public string BuscarEstadoUsuario(string codigo)
        {
            DataSet Res = Conexion.Conectar.EjecutarSelectMysql("select * from estado_usuario where cod_estado_usuario = " + codigo + ";");
            DataTable tab = Res.Tables[0];
            return JsonConvert.SerializeObject(tab, Formatting.None);
        }


        public bool ModificarEstadoUsuario(string codigo, string nombre)
        {
            string sql = "update estado_usuario set nombre_estado_usuario = '" + nombre + "' where cod_estado_usuario = " + codigo + ";";

            try
            {
                Conexion.Conectar.EjecutarOperacion(sql, lista, System.Data.CommandType.Text);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }


        public bool EliminarEstadoUsuario(string codigo)
        {
            string sql = "delete from estado_usuario where cod_estado_usuario = " + codigo + ";";

            try
            {
                Conexion.Conectar.EjecutarOperacion(sql, lista, System.Data.CommandType.Text);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }


        public string ListarUsuarios()
        {
            DataSet Res = Conexion.Conectar.EjecutarSelectMysql("select * from usuario;");
            DataTable tab = Res.Tables[0];
            return JsonConvert.SerializeObject(tab, Formatting.None);
        }


        public string BuscarUsuario(string codigo)
        {
            DataSet Res = Conexion.Conectar.EjecutarSelectMysql("select * from usuario where cod_usuario = " + codigo + ";");
            DataTable tab = Res.Tables[0];
            return JsonConvert.SerializeObject(tab, Formatting.None);
        }


        public bool ModificarUsuario(string codigo, string nombre, string apellido1, string apellido2, string pass, string programa, string tipousuario, string estado, string rol)
        {
            string sql = "update usuario set nombres = '" + nombre + "', apellido_1 = '"+apellido1+"', apellido_2 = '"+apellido2+"', pass = '"+pass+"', FK_programa = "+programa+", FK_tipo_usuario = "+tipousuario+", FK_estado_usuario = "+estado+", FK_rol = "+rol+" where cod_usuario = " + codigo + ";";

            try
            {
                Conexion.Conectar.EjecutarOperacion(sql, lista, System.Data.CommandType.Text);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }


        public bool EliminarUsuario(string codigo)
        {
            string sql = "delete from usuario where cod_usuario = " + codigo + ";";

            try
            {
                Conexion.Conectar.EjecutarOperacion(sql, lista, System.Data.CommandType.Text);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }


        public bool Login(string username, string pass)
        {
            DataSet res = Conexion.Conectar.EjecutarSelectMysql("select * from usuario where username = '" + username + "' and pass = '" + pass + "'; ");

            if (res.Tables[0].Rows.Count == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool CrearVotacion(string nombre, string descripcion, string fecha_inicio, string fecha_final, string tipo_usuario)
        {
            string sql = "insert into votacion (nombre, descripcion, fecha_inicio, fecha_final, FK_tipo_usuario) values ('" + nombre + "', '" + descripcion + "', '" + fecha_inicio + "', '" + fecha_final + "'," + tipo_usuario + "); ";

            try
            {
                Conexion.Conectar.EjecutarOperacion(sql, lista, System.Data.CommandType.Text);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool EliminarVotacion(string codigo)
        {
            string sql = "delete from votacion where cod_votacion = " + codigo + ";";

            try
            {
                Conexion.Conectar.EjecutarOperacion(sql, lista, System.Data.CommandType.Text);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool ModificarVotacion(string codigo, string nombre, string descripcion, string fecha_inicio, string fecha_final, string tipo_usuario)
        {
            string sql = "update votacion set nombre = '" + nombre + "', descripcion = '" + descripcion + "', fecha_inicio = '" + fecha_inicio + "', fecha_final = '" + fecha_final + "', fk_tipo_usuario = " + tipo_usuario + " where cod_votacion = " + codigo + "; ";

            try
            {
                Conexion.Conectar.EjecutarOperacion(sql, lista, System.Data.CommandType.Text);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool AgregarCandidato(string usuario, string votacion)
        {
            string sql = "insert into candidato_votacion (FK_usuario, FK_votacion) values (" + usuario + ", " + votacion + ");";


            try
            {
                Conexion.Conectar.EjecutarOperacion(sql, lista, System.Data.CommandType.Text);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
            
        }

        public bool EliminarCandidato(string usuario, string votacion)
        {
            string sql = "delete from candidato_votacion where fk_usuario = " + usuario + " and fk_votacion = " + votacion + ";";

            try
            {
                Conexion.Conectar.EjecutarOperacion(sql, lista, System.Data.CommandType.Text);
                return true;
            }
            catch (Exception e)
            {
                return false;

            }
        }


        public bool Votar(string usuario, string votacion)
        {
            string sql = "update candidato_votacion set total_votos = total_votos +1 where fk_usuario = " + usuario + " and fk_votacion = " + votacion + ";";

            try
            {
                Conexion.Conectar.EjecutarOperacion(sql, lista, System.Data.CommandType.Text);
                return true;
            }
            catch (Exception e)
            {
                return false;

            }
        }


        public String ProbarConexion()
        {
            DataSet Res = Conexion.Conectar.EjecutarSelectMysql("show full tables from votaua;");
            //DataSet Res = Conexion.Conectar.EjecutarSelectMysql("SHOW table status;"); 
            DataTable tab = Res.Tables[0];
            return JsonConvert.SerializeObject(tab, Formatting.None);

            //string sql = "show full tables from votaua;";
            //string ret = "a";

            //try
            //{
            //    Conexion.Conectar.EjecutarSelectMysql(sql);
                
            //    ret = "consulta hecha"; 
            //}
            //catch (Exception e)
            //{
            //    ret = "Error: " + e.Message; 

            //}

            //return ret; 
        }
    }
}
