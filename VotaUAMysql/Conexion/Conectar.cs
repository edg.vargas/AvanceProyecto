﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using System.Data;

namespace VotaUAMysql.Conexion
{
    public class Conectar
    {

        //private static MySqlConnection conexion = new MySqlConnection("Server= localhost; Database= votaua; uid= root; password= root;");

        //public static string cad_Conexion()
        //{
        //    return "Server= localhost; Database= votaua; uid= root; password= root;";

        //}

        private static MySqlConnection conexion = new MySqlConnection("Server= 208.91.199.11; Port=3306; Database= votaua; uid= edgaroswaldo; password= 3dg4r0sw4ld@_; ");

        public static string cad_Conexion()
        {
            return "Server= 208.91.199.11; Port=3306; Database= votaua; uid= edgaroswaldo; password= 3dg4r0sw4ld@_; ";

        }

        public static MySqlConnection ConectarMysql() //Metoto para conectar a C# a MySQL
        {
            string CadenaConexion;
            CadenaConexion = cad_Conexion();
            MySqlConnection Conexion = new MySqlConnection(CadenaConexion);

            try
            {
                Conexion.Open();
            }
            catch (Exception error)
            {
                //   MessageBox.Show("Error de configuración del sistema " + error.Message, "Aplicación : ", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                //   Application.Exit();
                throw new Exception(error.Message);
            }
            return Conexion;
        }


        public static DataSet EjecutarSelectMysql(string p)
        {
            System.Data.DataSet dt = new System.Data.DataSet();

            MySqlConnection Conn = ConectarMysql();
            MySqlCommand ComandoSelect = new MySqlCommand(p);
            ComandoSelect.Connection = Conn;

            // MySqlDataReader Resultado;
            MySqlDataAdapter da = new MySqlDataAdapter(p, Conn);
            //da = ComandoSelect.ExecuteReader();

            da.Fill(dt);


            Conn.Close();
            //c = "llego";
            return dt;
        }



        public static void EjecutarOperacion(string sentencia, List<MySqlParameter> ListaParametros, CommandType TipoComando)
        {
            if (ConectarOpen())
            {
                //vambio el void

                MySqlCommand comando = new MySqlCommand();
                //comando.CommandText = "insert into persona(id, nombre, apellido, mail, telefono) values('678', 'luis','marc','ucu@osdc.ca','235')" ;
                //comando.CommandText = "insert into materias(id_materia, Nombre, profesor, semestre) values('34','politica','juanes','3')";
                comando.CommandText = sentencia;
                comando.CommandType = TipoComando;
                comando.Connection = conexion;
                foreach (MySqlParameter parametro in ListaParametros)
                {
                    comando.Parameters.Add(parametro);
                }

                //pongo esto en try carch para que al exexutenonquery() 
                //le capture el error y asi haga el desconectar() de todas formas 
                try
                {
                    comando.ExecuteNonQuery();
                    Desconectar();

                }
                catch (Exception q)
                {
                    Desconectar();
                    throw new Exception(q.Message);
                }

            }
            else
            {

                throw new Exception("No ha sido posible conectarse a la Base de Datos");

            }


        }

        private static void Desconectar()
        {
            conexion.Close();
        }

        private static bool ConectarOpen()
        {

            try
            {
                conexion.Open();

                return true;
            }
            catch (Exception)
            {

                return false;
            }

        }

    }
}